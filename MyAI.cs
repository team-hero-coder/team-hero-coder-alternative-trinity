using System;
using System.Collections.Generic;
using TeamHeroCoderLibrary;

public static class MyAI
{
    public static string FolderExchangePath = "Replace this text with the Team Hero Coder exchange directory";

    static bool hasPerformedAction;

    static FoeEvaluation foeEvaluation;

    public static void ProcessAI()
    {
        hasPerformedAction = false;
        foeEvaluation = CreateFoeEvaluation();

        switch (TeamHeroCoder.BattleState.heroWithInitiative.jobClass)
        {
            case HeroJobClass.Rogue:
                ProcessRogueAI();
                break;

            case HeroJobClass.Monk:
                ProcessMonkAI();
                break;

            case HeroJobClass.Alchemist:
                ProcessAlchemistAI();
                break;

            default:
                Console.WriteLine("Hero character class not found!");
                break;
        }

        #region If no action has been performed, default to attack foe with lowest health

        if (!hasPerformedAction)
        {
            Console.WriteLine("No action was found, performing default attack vs lowest health foe");
            List<Hero> heroesNotProtectedByCover = Evaluation.FilterOutHeroesThatAreProtectedByCover(TeamHeroCoder.BattleState.foeHeroes);
            Hero targetWithLowestHealthThatIsNotProtectedByCover = Evaluation.FindStandingHeroWithLowestHealth(heroesNotProtectedByCover);
            TeamHeroCoder.PerformHeroAbility(Ability.Attack, targetWithLowestHealthThatIsNotProtectedByCover);
        }

        #endregion

    }

    static private void CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability ability, Hero? target = null)
    {
        if (!hasPerformedAction && Utility.AreAbilityAndTargetLegal(ability, target, false))
        {
            TeamHeroCoder.PerformHeroAbility(ability, target);
            hasPerformedAction = true;
        }
    }

    static public void ProcessRogueAI()
    {

        List<Hero> tempHeroList;

        if (foeEvaluation.goldCostOfItemsThatWillBeStolen >= Info.ItemCost.Elixir + Info.ItemCost.Potion)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Steal, null);

        EmergencyItemUse();

        if (foeEvaluation.goldCostOfItemsThatWillBeStolen >= Info.ItemCost.Ether)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Steal, null);

        #region Remedies

        tempHeroList = Evaluation.FindHeroesWithNegativeStatusEffectsMoreThanAmount(1, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.FullRemedy, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Petrified, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.PetrifyRemedy, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Silence, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Alchemist, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.SilenceRemedy, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Doom, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.FullRemedy, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Petrifying, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.PetrifyRemedy, tempHeroList[0]);

        #endregion

        #region Attack Will KO

        foreach (Hero h in TeamHeroCoder.BattleState.foeHeroes)
        {
            if (h.health == 0)
                continue;

            int dmgDealt = Utility.CalculateDamageAmount(TeamHeroCoder.BattleState.heroWithInitiative, Info.AbilityModifersAndParams.AttackDamageMod, h, true);
            
            if (dmgDealt >= h.health)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Attack, h);
        }

        #endregion

        if (foeEvaluation.goldCostOfItemsThatWillBeStolen >= Info.ItemCost.Potion)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Steal, null);

        #region Stun Strike

        tempHeroList = Evaluation.FindStandingHeroesWithInitiativeGreaterThanAmount(45f, TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesPerkCountMoreThanAmount(2, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.StunStrike, tempHeroList[0]);

        tempHeroList = Evaluation.FindStandingHeroesWithInitiativeGreaterThanAmount(45f, TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesPerkCountMoreThanAmount(1, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.StunStrike, tempHeroList[0]);

        #endregion

        LowPriorityEtherAndElixirUse();

        if (foeEvaluation.numberOfLowGoldCostItems < 7 && foeEvaluation.goldCostOfHighValueItems > Info.ItemCost.Elixir * 2)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Steal, null);


        Hero lh = Evaluation.FindStandingHeroWithLowestHealth(TeamHeroCoder.BattleState.foeHeroes);
        CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.StunStrike, lh);

    }

    static public void ProcessMonkAI()
    {
        List<Hero> tempHeroList;

        EmergencyItemUse();

        #region Counter Play vs Mana Burn Locking Alechemist Out of Mana

        int etherCount = Evaluation.GetCountOfItemInInventory(Item.Ether, TeamHeroCoder.BattleState.allyInventory);
        if (etherCount < 1)
        {
            tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Alchemist, TeamHeroCoder.BattleState.allyHeroes);
            tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.2f, tempHeroList);
            if (tempHeroList.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Chakra, tempHeroList[0]);
        }

        #endregion

        #region Flurry/Attack Will KO

        foreach(Hero h in TeamHeroCoder.BattleState.foeHeroes)
        {
            if (h.health == 0)
                continue;

            int dmgDealt = Utility.CalculateDamageAmount(TeamHeroCoder.BattleState.heroWithInitiative, Info.AbilityModifersAndParams.FlurryOfBlowsDamageMod, h, true);

            if (dmgDealt >= h.health)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.FlurryOfBlows, null);
        }

        tempHeroList = Evaluation.FilterOutHeroesThatAreProtectedByCover(TeamHeroCoder.BattleState.foeHeroes);
        foreach (Hero h in tempHeroList)
        {
            if (h.health == 0)
                continue;

            int dmgDealt = Utility.CalculateDamageAmount(TeamHeroCoder.BattleState.heroWithInitiative, Info.AbilityModifersAndParams.AttackDamageMod, h, true);

            if (dmgDealt >= h.health)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Attack, h);
        }

        #endregion

        #region Debuffs vs high DPS Targets

        tempHeroList = Evaluation.FindStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Defaith, tempHeroList);
        tempHeroList = Evaluation.FindHeroesWithPerk(Perk.WizardMastery, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Defaith, tempHeroList[0]);

        tempHeroList = Evaluation.FindStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Debrave, tempHeroList);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Monk, tempHeroList);
        tempHeroList = Evaluation.FindHeroesPerkCountMoreThanAmount(2, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Debrave, tempHeroList[0]);

        tempHeroList = Evaluation.FindStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Debrave, tempHeroList);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Fighter, tempHeroList);
        tempHeroList = Evaluation.FindHeroesPerkCountMoreThanAmount(2, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Debrave, tempHeroList[0]);

        tempHeroList = Evaluation.FindStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Defaith, tempHeroList);
        tempHeroList = Evaluation.FindHeroesWithPerk(Perk.WizardEvoker, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Defaith, tempHeroList[0]);

        #endregion

        tempHeroList = Evaluation.FindStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 1)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.FlurryOfBlows, null);

        #region Ether, Elixir and Chakra Play

        LowPriorityEtherAndElixirUse();

        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Alchemist, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.2f, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Chakra, tempHeroList[0]);

        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.2f, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Chakra, tempHeroList[0]);

        #endregion

    }

    static public void ProcessAlchemistAI()
    {
        List<Hero> tempHeroList, tempHeroList2;

        EmergencyItemUse();

        #region Situational Item Crafting

        if (foeEvaluation.hasBurstDamage)
        {
            if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.Potion))
            {
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftPotion, null);
            }

            if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.Revive))
            {
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftRevive, null);
            }

            if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.MegaElixir))
            {
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftMegaElixir, null);
            }
        }

        if (foeEvaluation.hasSilencing)
        {
            if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.SilenceRemedy))
            {
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftSilenceRemedy, null);
            }
        }

        if (foeEvaluation.hasPetrify)
        {
            if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(2, Item.PetrifyRemedy))
            {
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftPetrifyRemedy, null);
            }
        }

        if (foeEvaluation.hasStealing)
        {
            if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.PoisonRemedy))
            {
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftPoisonRemedy, null);
            }
        }

        if (foeEvaluation.hasDoom)
        {
            if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.FullRemedy))
            {
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftFullRemedy, null);
            }
        }

        if (foeEvaluation.hasPoisonTribal)
        {
            if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.PoisonRemedy))
            {
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftPoisonRemedy, null);
            }
        }

        #endregion

        #region High Value Dispel/Cleanse

        tempHeroList = Evaluation.FindHeroesWithNegativeStatusEffectsMoreThanAmount(1, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Cleanse, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithPositiveStatusEffectsMoreThanAmount(1, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Dispel, tempHeroList[0]);

        #endregion

        #region Attack Will KO

        tempHeroList = Evaluation.FilterOutHeroesThatAreProtectedByCover(TeamHeroCoder.BattleState.foeHeroes);
        foreach (Hero h in tempHeroList)
        {
            if (h.health == 0)
                continue;

            int dmgDealt = Utility.CalculateDamageAmount(TeamHeroCoder.BattleState.heroWithInitiative, Info.AbilityModifersAndParams.AttackDamageMod, h, true);
            
            if (dmgDealt >= h.health)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Attack, h);
        }

        #endregion

        #region Core Battle Transition Crafting

        if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.Potion))
        {
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftPotion, null);
        }

        if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.Revive))
        {
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftRevive, null);

        }

        if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.Ether))
        {

            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftEther, null);

        }

        if (Evaluation.PlayerInventoryHasLessThanAmountOfItem(1, Item.Elixir))
        {

            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CraftElixir, null);

        }

        #endregion

        #region Haste and Slow

        if (!foeEvaluation.hasQuickDispel)
        {
            tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Rogue, TeamHeroCoder.BattleState.allyHeroes);
            tempHeroList = Evaluation.FindHeroesPerkCountMoreThanAmount(2, tempHeroList);
            tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Haste, tempHeroList);

            if (tempHeroList.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Haste, tempHeroList[0]);

            tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Alchemist, TeamHeroCoder.BattleState.allyHeroes);
            tempHeroList = Evaluation.FindHeroesPerkCountMoreThanAmount(2, tempHeroList);
            tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Haste, tempHeroList);

            if (tempHeroList.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Haste, tempHeroList[0]);
        }

        if (!foeEvaluation.hasQuickCleanse)
        {
            tempHeroList = Evaluation.FindHeroesPerkCountMoreThanAmount(2, TeamHeroCoder.BattleState.foeHeroes);
            tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Slow, tempHeroList);

            if (tempHeroList.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Slow, tempHeroList[0]);

            tempHeroList = Evaluation.FindHeroesPerkCountMoreThanAmount(1, TeamHeroCoder.BattleState.foeHeroes);
            tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Slow, tempHeroList);

            if (tempHeroList.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Slow, tempHeroList[0]);
        }


        #endregion

        LowPriorityEtherAndElixirUse();

        tempHeroList = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.5f, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Potion, tempHeroList[0]);

    }

    static public FoeEvaluation CreateFoeEvaluation()
    {
        FoeEvaluation foeEvaluation = new FoeEvaluation();
        List<Hero> tempHeroList;
        float hint = 0;

        #region Evaluate Has Stealing

        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Rogue, TeamHeroCoder.BattleState.foeHeroes);
        foeEvaluation.hasStealing = tempHeroList.Count > 0;

        #endregion

        #region Evaluate Has Petrify

        //Console.WriteLine("******************************************");

        foreach (RecordOfAbilityUse rba in TeamHeroCoder.BattleState.battleReport)
        {
            bool heroIsFoe = TeamHeroCoder.BattleState.foeHeroes.Contains(rba.heroWithInitiative);
            //bool targetedWasAlly = TeamHeroCoder.BattleState.playerHeroes.Contains(rba.heroBeingTargeted);
            bool petrifyWasUsed = rba.ability == Ability.Petrify;

            //Console.WriteLine("targetedWasAlly == " + targetedWasAlly + ", petrifyWasUsed == " + petrifyWasUsed);

            if (petrifyWasUsed)//heroIsFoe && 
                foeEvaluation.hasPetrify = true;
        }

        #endregion

        #region Evaluate Has Doom

        foreach (RecordOfAbilityUse rba in TeamHeroCoder.BattleState.battleReport)
        {
            //bool heroIsFoe = TeamHeroCoder.BattleState.foeHeroes.Contains(rba.heroWithInitiative);
            bool doomWasUsed = rba.ability == Ability.Doom;

            if (doomWasUsed)//heroIsFoe && 
                foeEvaluation.hasDoom = true;
        }

        #endregion

        #region Evaluate Has Poison Tribal

        bool hasPoison = false;
        tempHeroList = Evaluation.FindHeroesWithAbility(Ability.PoisonNova, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            hasPoison = true;
        tempHeroList = Evaluation.FindHeroesWithAbility(Ability.PoisonStrike, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            hasPoison = true;

        if (hasPoison)
        {
            tempHeroList = Evaluation.FindHeroesWithPassiveAbility(PassiveAbility.BitterBloom, TeamHeroCoder.BattleState.foeHeroes);
            if(tempHeroList.Count > 1)
                foeEvaluation.hasPoisonTribal = true;
        }

        #endregion

        #region Evaluate Has Silencing

        foreach (RecordOfAbilityUse rba in TeamHeroCoder.BattleState.battleReport)
        {
            //bool heroIsFoe = TeamHeroCoder.BattleState.foeHeroes.Contains(rba.heroWithInitiative);
            bool silenceStrikeWasUsed = rba.ability == Ability.SilenceStrike;

            if (silenceStrikeWasUsed)//heroIsFoe && 
                foeEvaluation.hasSilencing = true;
        }

        #endregion

        #region Evaluate Has Chakra Sustain

        tempHeroList = Evaluation.FindHeroesWithAbility(Ability.Chakra, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            foeEvaluation.hasChakraSustain = true;

        #endregion

        #region Evaluate Has Item Craft Sustain

        tempHeroList = Evaluation.FindHeroesWithPerk(Perk.AlchemistAetherist, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            foeEvaluation.hasItemCraftSustain = true;

        #endregion

        #region Evaluate Has Burst Damage

        hint = 0;

        tempHeroList = Evaluation.FindHeroesWithPerk(Perk.WizardMastery, TeamHeroCoder.BattleState.foeHeroes);
        hint = hint + ((float)tempHeroList.Count * 2.5f);
        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Faith, tempHeroList);
        hint = hint + ((float)tempHeroList.Count * 2.5f);

        tempHeroList = Evaluation.FindHeroesWithPerk(Perk.MonkBoundlessFist, TeamHeroCoder.BattleState.foeHeroes);
        hint = hint + ((float)tempHeroList.Count * 1.5f);
        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Brave, tempHeroList);
        hint = hint + ((float)tempHeroList.Count * 1.5f);

        tempHeroList = Evaluation.FindHeroesWithPerk(Perk.FighterSamurai, TeamHeroCoder.BattleState.foeHeroes);
        hint = hint + ((float)tempHeroList.Count * 1f);
        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Brave, tempHeroList);
        hint = hint + ((float)tempHeroList.Count * 1f);

        if (hint >= 5)
            foeEvaluation.hasBurstDamage = true;

        #endregion

        #region Evaluate Has Quick Dispel

        tempHeroList = Evaluation.FindHeroesWithAbility(Ability.QuickDispel, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            foeEvaluation.hasQuickDispel = true;

        #endregion

        #region Evaluate Has Quick Cleanse

        tempHeroList = Evaluation.FindHeroesWithAbility(Ability.QuickCleanse, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            foeEvaluation.hasQuickCleanse = true;

        #endregion

        #region Evaluate For Stealing

        foeEvaluation.goldCostOfItemsThatWillBeStolen = Evaluation.GoldCostOfItemsThatWillBeStolen(TeamHeroCoder.BattleState.heroWithInitiative, TeamHeroCoder.BattleState.foeInventory);
        foeEvaluation.goldCostOfHighValueItems = Evaluation.GoldCostOfHighValueItem(TeamHeroCoder.BattleState.foeInventory);
        foeEvaluation.numberOfLowGoldCostItems = Evaluation.CountNumberOfLowValueItems(TeamHeroCoder.BattleState.foeInventory);

        #endregion

        return foeEvaluation;
    }

    static private void EmergencyItemUse()
    {
        List<Hero> slainAllyHeroes = Evaluation.FindSlainHeroes(TeamHeroCoder.BattleState.allyHeroes);
        List<Hero> allyHeroesWithHealthLessThan30Percent = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.3f, TeamHeroCoder.BattleState.allyHeroes);
        List<Hero> tempHeroList;

        if (allyHeroesWithHealthLessThan30Percent.Count > 1)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.MegaElixir, null);

        if (slainAllyHeroes.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Revive, slainAllyHeroes[0]);

        if (allyHeroesWithHealthLessThan30Percent.Count > 0)
        {
            tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.5f, allyHeroesWithHealthLessThan30Percent);
            if (tempHeroList.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Elixir, tempHeroList[0]);
            else
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Potion, allyHeroesWithHealthLessThan30Percent[0]);
        }

    }

    static private void LowPriorityEtherAndElixirUse()
    {
        List<Hero> tempHeroList, tempHeroList2;

        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Alchemist, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.25f, tempHeroList);
        if (tempHeroList.Count > 0)
        {
            tempHeroList2 = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.3f, tempHeroList);

            if (tempHeroList2.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Elixir, tempHeroList2[0]);

            if(!hasPerformedAction)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);
        }

        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Rogue, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.25f, tempHeroList);
        if (tempHeroList.Count > 0)
        {
            tempHeroList2 = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.3f, tempHeroList);

            if (tempHeroList2.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Elixir, tempHeroList2[0]);

            if (!hasPerformedAction)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);
        }

        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Monk, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.25f, tempHeroList);
        if (tempHeroList.Count > 0)
        {
            tempHeroList2 = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.3f, tempHeroList);

            if (tempHeroList2.Count > 0)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Elixir, tempHeroList2[0]);

            if (!hasPerformedAction)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);
        }
    }

}


public class FoeEvaluation
{
    public bool hasStealing;
    public bool hasPetrify;
    public bool hasDoom;
    public bool hasPoisonTribal;
    public bool hasSilencing;
    public bool hasChakraSustain;
    public bool hasItemCraftSustain;
    public bool hasBurstDamage;
    public bool hasQuickDispel;
    public bool hasQuickCleanse;
    public int goldCostOfItemsThatWillBeStolen;
    public int goldCostOfHighValueItems;
    public int numberOfLowGoldCostItems;
}




// private void ProcessFighterAI()
// {
//     Console.WriteLine("Processing AI for fighter");

//     Hero fighterWithInitiative = TeamHeroCoder.BattleState.heroWithInitiative;
//     List<Hero> tempHeroList;
//     tempHeroList = Evaluation.FilterOutHeroesThatAreProtectedByCover(TeamHeroCoder.BattleState.foeHeroes);
//     Hero targetWithLowestHealthThatIsNotProtectedByCover = Evaluation.FindStandingHeroWithLowestHealth(tempHeroList);

//     #region Ether Usage

//     tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Ether, tempHeroList[0]);

//     tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Ether, tempHeroList[0]);

//     #endregion

//     #region Silence Rememdy Usage

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Silence, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.SilenceRemedy, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Silence, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.SilenceRemedy, tempHeroList[0]);

//     #endregion

//     bool hasBraveSE = Evaluation.HeroHasStatusEffect(TeamHeroCoder.StatusEffectID.Brave, fighterWithInitiative);
//     if (!hasBraveSE)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Brave, fighterWithInitiative);

//     if (hasBraveSE)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickHit, targetWithLowestHealthThatIsNotProtectedByCover);

//     CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Attack, targetWithLowestHealthThatIsNotProtectedByCover);

// }

// private void ProcessClericAI()
// {
//     Console.WriteLine("Processing AI for cleric");

//     List<Hero> slainAllyHeroes = Evaluation.FindSlainHeroes(TeamHeroCoder.BattleState.playerHeroes);
//     List<Hero> allyHeroesWithHealthLessThan30Percent = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.3f, TeamHeroCoder.BattleState.playerHeroes);
//     List<Hero> allyHeroesWithHealthLessThan50Percent = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.5f, TeamHeroCoder.BattleState.playerHeroes);
//     List<Hero> allyHeroesWithHealthLessThan70Percent = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.7f, TeamHeroCoder.BattleState.playerHeroes);
//     List<Hero> tempHeroList;

//     #region Emergency Heals/Resurrect

//     if (slainAllyHeroes.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Resurrection, slainAllyHeroes[0]);

//     if (allyHeroesWithHealthLessThan30Percent.Count >= 2)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.MassHeal, allyHeroesWithHealthLessThan30Percent[0]);

//     if (allyHeroesWithHealthLessThan30Percent.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.CureSerious, allyHeroesWithHealthLessThan30Percent[0]);

//     #endregion

//     #region Quick Cleanse

//     tempHeroList = Evaluation.FindHeroesWithNegativeStatusEffectsMoreThanAmount(1, TeamHeroCoder.BattleState.playerHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickCleanse, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Slow, TeamHeroCoder.BattleState.playerHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickCleanse, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Petrified, TeamHeroCoder.BattleState.playerHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickCleanse, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Doom, TeamHeroCoder.BattleState.playerHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickCleanse, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Petrifying, TeamHeroCoder.BattleState.playerHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickCleanse, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Defaith, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickCleanse, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Defaith, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickCleanse, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Debrave, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Fighter, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickCleanse, tempHeroList[0]);

//     #endregion

//     #region Mid Importance Heals

//     if (allyHeroesWithHealthLessThan50Percent.Count >= 2)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.MassHeal, allyHeroesWithHealthLessThan50Percent[0]);

//     if (allyHeroesWithHealthLessThan50Percent.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickHeal, allyHeroesWithHealthLessThan50Percent[0]);

//     #endregion

//     #region Ether Usage

//     tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Ether, tempHeroList[0]);

//     tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Ether, tempHeroList[0]);

//     #endregion

//     #region Silence Rememdy Usage

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Silence, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.SilenceRemedy, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Silence, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.SilenceRemedy, tempHeroList[0]);

//     #endregion

//     #region Positive Status Effects

//     tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(TeamHeroCoder.StatusEffectID.Faith, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Faith, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(TeamHeroCoder.StatusEffectID.Brave, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Fighter, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Brave, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(TeamHeroCoder.StatusEffectID.Faith, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Faith, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(TeamHeroCoder.StatusEffectID.Haste, TeamHeroCoder.BattleState.playerHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Haste, tempHeroList[0]);

//     #endregion

//     #region Low Importance Heals

//     if (allyHeroesWithHealthLessThan70Percent.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickHeal, allyHeroesWithHealthLessThan70Percent[0]);

//     if (allyHeroesWithHealthLessThan70Percent.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.CureLight, allyHeroesWithHealthLessThan70Percent[0]);

//     #endregion

// }

// private void ProcessWizardAI()
// {
//     Console.WriteLine("Processing AI for wizard");

//     Hero target;
//     int targetCount;
//     List<Hero> tempHeroList;
//     Hero wizardWithInitiative = TeamHeroCoder.BattleState.heroWithInitiative;

//     if (Evaluation.HeroHasStatusEffect(TeamHeroCoder.StatusEffectID.Faith, wizardWithInitiative))
//     {
//         targetCount = Evaluation.CountNumberOfStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
//         if (targetCount > 2)
//             CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Meteor);
//     }

//     #region Quick Dispel

//     tempHeroList = Evaluation.FindHeroesWithPositiveStatusEffectsMoreThanAmount(1, TeamHeroCoder.BattleState.foeHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickDispel, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Haste, TeamHeroCoder.BattleState.foeHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickDispel, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.AutoLife, TeamHeroCoder.BattleState.foeHeroes);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickDispel, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Faith, TeamHeroCoder.BattleState.foeHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickDispel, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Faith, TeamHeroCoder.BattleState.foeHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickDispel, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Brave, TeamHeroCoder.BattleState.foeHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Fighter, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickDispel, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Brave, TeamHeroCoder.BattleState.foeHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Monk, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.QuickDispel, tempHeroList[0]);

//     #endregion

//     #region Ether Usage

//     tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Ether, tempHeroList[0]);

//     tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Ether, tempHeroList[0]);

//     #endregion

//     #region Silence Rememdy Usage

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Silence, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Cleric, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.SilenceRemedy, tempHeroList[0]);

//     tempHeroList = Evaluation.FindHeroesWithStatusEffect(TeamHeroCoder.StatusEffectID.Silence, TeamHeroCoder.BattleState.playerHeroes);
//     tempHeroList = Evaluation.FindHeroesWithCharacterClass(TeamHeroCoder.HeroClassID.Wizard, tempHeroList);
//     if (tempHeroList.Count > 0)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.SilenceRemedy, tempHeroList[0]);

//     #endregion

//     targetCount = Evaluation.CountNumberOfStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
//     if (targetCount > 2)
//         CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.Fireball);

//     target = Evaluation.FindStandingHeroWithLowestHealth(TeamHeroCoder.BattleState.foeHeroes);
//     CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.FlameStrike, target);

//     target = Evaluation.FindStandingHeroWithLowestHealth(TeamHeroCoder.BattleState.foeHeroes);
//     CheckIfAbilityAndTargetAreLegalAndPerformIfSo(TeamHeroCoder.AbilityID.MagicMissile, target);
// }

