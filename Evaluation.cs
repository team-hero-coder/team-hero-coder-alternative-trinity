using System;
using System.Collections.Generic;
using TeamHeroCoderLibrary;
using static TeamHeroCoderLibrary.TeamHeroCoder;

public static class Evaluation
{
    public static bool HeroHasStatusEffect(StatusEffect statusEffect, Hero hero)
    {
        foreach (StatusEffectAndDuration se in hero.statusEffectsAndDurations)
        {
            if (se.statusEffect == statusEffect)
            {
                return true;
            }
        }

        return false;
    }

    public static List<Hero> FindHeroesWithStatusEffect(StatusEffect statusEffect, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithStatusEffect = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (HeroHasStatusEffect(statusEffect, hero))
                heroesWithStatusEffect.Add(hero);
        }

        return heroesWithStatusEffect;
    }

    public static List<Hero> FindHeroesWithoutStatusEffect(StatusEffect statusEffect, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithStatusEffect = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (!HeroHasStatusEffect(statusEffect, hero))
                heroesWithStatusEffect.Add(hero);
        }

        return heroesWithStatusEffect;
    }

    public static List<Hero> FindStandingHeroesWithHealthLessThanAmount(float amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithHealthLessThanAmount = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                continue;

            float p = (float)hero.health / (float)hero.maxHealth;
            if (p < amount)
                heroesWithHealthLessThanAmount.Add(hero);
        }

        return heroesWithHealthLessThanAmount;
    }

    public static List<Hero> FindStandingHeroesWithManaLessThanAmount(float amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithManaLessThanAmount = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                continue;

            if (hero.maxMana == 0)
                continue;

            float p = (float)hero.mana / (float)hero.maxMana;
            if (p < amount)
                heroesWithManaLessThanAmount.Add(hero);
        }

        return heroesWithManaLessThanAmount;
    }

    public static Hero FindStandingHeroWithLowestHealth(List<Hero> teamOfHeroes)
    {
        Hero lowestHealth = null;

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                continue;

            if (lowestHealth == null)
                lowestHealth = hero;
            else if (hero.health < lowestHealth.health)
                lowestHealth = hero;
        }

        return lowestHealth;
    }

    public static int CountNumberOfStandingHeroes(List<Hero> teamOfHeroes)
    {
        int count = 0;

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health > 0)
                count++;
        }

        return count;
    }

    public static List<Hero> FindSlainHeroes(List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithHealthLessThanAmount = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                heroesWithHealthLessThanAmount.Add(hero);
        }

        return heroesWithHealthLessThanAmount;
    }

    public static List<Hero> FindHeroesWithCharacterClass(HeroJobClass jobClass, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithCharacterClass = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.jobClass == jobClass)
                heroesWithCharacterClass.Add(hero);
        }

        return heroesWithCharacterClass;
    }

    private static List<StatusEffect> NegativeStatusEffects = new List<StatusEffect>(){
        StatusEffect.Debrave,
        StatusEffect.Defaith,
        StatusEffect.Doom,
        StatusEffect.Petrified,
        StatusEffect.Slow,
        StatusEffect.Silence
    };

    private static List<StatusEffect> PositiveStatusEffects = new List<StatusEffect>(){
        StatusEffect.Brave,
        StatusEffect.Faith,
        StatusEffect.AutoLife,
        StatusEffect.Haste,
    };

    public static int CountNumberOfNegativeStatusEffects(Hero hero)
    {
        int count = 0;
        foreach (StatusEffectAndDuration se in hero.statusEffectsAndDurations)
        {
            if (NegativeStatusEffects.Contains(se.statusEffect))
                count++;
        }

        return count;
    }

    public static int CountNumberOfPositiveStatusEffects(Hero hero)
    {
        int count = 0;
        foreach (StatusEffectAndDuration se in hero.statusEffectsAndDurations)
        {
            if (PositiveStatusEffects.Contains(se.statusEffect))
                count++;
        }

        return count;
    }

    public static List<Hero> FindHeroesWithNegativeStatusEffectsMoreThanAmount(int amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithNegStatusEffectMoreThan = new List<Hero>();
        foreach (Hero hero in teamOfHeroes)
        {
            if (CountNumberOfNegativeStatusEffects(hero) > amount)
                heroesWithNegStatusEffectMoreThan.Add(hero);
        }

        return heroesWithNegStatusEffectMoreThan;
    }

    public static List<Hero> FindHeroesWithPositiveStatusEffectsMoreThanAmount(int amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithPosStatusEffectMoreThan = new List<Hero>();
        foreach (Hero hero in teamOfHeroes)
        {
            if (CountNumberOfPositiveStatusEffects(hero) > amount)
                heroesWithPosStatusEffectMoreThan.Add(hero);
        }

        return heroesWithPosStatusEffectMoreThan;
    }

    public static List<Hero> FilterOutHeroesThatAreProtectedByCover(List<Hero> teamOfHeroes)
    {
        bool teamHasStandingHeroWithCover = false;

        List<Hero> heroesNotProtectedByCover = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health > 0 && hero.passiveAbilities.Contains(PassiveAbility.Cover))
                teamHasStandingHeroWithCover = true;
        }

        foreach (Hero hero in teamOfHeroes)
        {
            bool heroCanBeProtectedByCover = (hero.jobClass == HeroJobClass.Cleric || hero.jobClass == HeroJobClass.Wizard);
            if (teamHasStandingHeroWithCover && heroCanBeProtectedByCover)
                continue;
            else
                heroesNotProtectedByCover.Add(hero);
        }

        return heroesNotProtectedByCover;
    }

    public static List<Hero> FindHeroesWithPerk(Perk perk, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithPerkID = new List<Hero>();

        foreach (Hero h in teamOfHeroes)
        {
            foreach(Perk p in h.perks)
            {
                if (p == perk)
                    heroesWithPerkID.Add(h);
            }
            
        }

        return heroesWithPerkID;
    }


    public static List<Hero> FindStandingHeroes(List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithManaLessThanAmount = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                continue;

            heroesWithManaLessThanAmount.Add(hero);
        }

        return heroesWithManaLessThanAmount;
    }


    public static List<Hero> FindHeroesPerkCountMoreThanAmount(int amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithPerkID = new List<Hero>();

        foreach (Hero h in teamOfHeroes)
        {
            if (h.perks.Count > amount)
                heroesWithPerkID.Add(h);
        }

        return heroesWithPerkID;
    }

    public static bool PlayerInventoryHasLessThanAmountOfItem(int amount, Item item)
    {
        foreach(InventoryItem ii in TeamHeroCoder.BattleState.allyInventory)
        {
            if(ii.item == item)
            {
                if (ii.count < amount)
                    return true;
                else
                    return false;
            }
        }

        return true;
    }

    private static bool DoesHeroHavePassiveAbility(PassiveAbility passiveAbility, Hero hero)
    {
        foreach (PassiveAbility p in hero.passiveAbilities)
        {
            if (p == passiveAbility)
                return true;
        }

        return false;
    }

    public static List<Hero> FindHeroesWithPassiveAbility(PassiveAbility passiveAbility, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithPassiveAbilityID = new List<Hero>();

        foreach (Hero h in teamOfHeroes)
        {
            if (DoesHeroHavePassiveAbility(passiveAbility, h))
            {
                heroesWithPassiveAbilityID.Add(h);
            }
        }

        return heroesWithPassiveAbilityID;
    }

    public static List<Hero> FindHeroesWithAbility(Ability ability, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithAbilityID = new List<Hero>();

        foreach (Hero h in teamOfHeroes)
        {
            foreach (Ability a in h.abilities)
            {
                if (a == ability)
                    heroesWithAbilityID.Add(h);
            }
        }

        return heroesWithAbilityID;
    }


    public static int GoldCostOfItemsThatWillBeStolen(Hero heroPerformingSteal, List<InventoryItem> inventoryBeingStolenFrom)
    {
        int goldValueOfCheapestItem = 0;
        int goldValueOfSecondCheapestItem = 0;
        bool hasLarceny = DoesHeroHavePassiveAbility(PassiveAbility.Larceny, heroPerformingSteal);

        foreach (InventoryItem ii in inventoryBeingStolenFrom)
        {
            if (ii.item == Item.Essence || ii.count == 0)
                continue;

            int c = Info.ItemCost.lookUpCostValueFromID[ii.item];

            if (goldValueOfCheapestItem == 0 || goldValueOfCheapestItem > c)
            {
                if (goldValueOfCheapestItem > 0)
                    goldValueOfSecondCheapestItem = goldValueOfCheapestItem;

                goldValueOfCheapestItem = c;

                if(ii.count > 1)
                    goldValueOfSecondCheapestItem = c;
            }
        }

        //Console.WriteLine("goldValueOfCheapestItem == " + goldValueOfCheapestItem + "goldValueOfSecondCheapestItem == " + goldValueOfSecondCheapestItem + ", hasLarceny == " + hasLarceny);

        if (hasLarceny)
            return goldValueOfCheapestItem + goldValueOfSecondCheapestItem;
        else
            return goldValueOfCheapestItem;
    }

    public static int GoldCostOfHighValueItem(List<InventoryItem> inventory)
    {
        int totalGoldCostOfHighValueItems = 0;

        foreach (InventoryItem ii in inventory)
        {
            if (ii.item == Item.Essence || ii.count == 0)
                continue;
            int c = Info.ItemCost.lookUpCostValueFromID[ii.item];

            if (c >= Info.ItemCost.Potion)
            {
                totalGoldCostOfHighValueItems += c;
            }
        }

        return totalGoldCostOfHighValueItems;
    }

    public static List<Hero> FindStandingHeroesWithInitiativeGreaterThanAmount(float amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithInitiativePercentGreaterThanAmount = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                continue;

            if (hero.initiativePercent > amount)
                heroesWithInitiativePercentGreaterThanAmount.Add(hero);
        }

        return heroesWithInitiativePercentGreaterThanAmount;
    }


    public static int CountNumberOfLowValueItems(List<InventoryItem> inventory)
    {
        int numberOfLowValueItems = 0;

        foreach (InventoryItem ii in inventory)
        {
            if (ii.item == Item.Essence || ii.count == 0)
                continue;
            int c = Info.ItemCost.lookUpCostValueFromID[ii.item];

            if (c < Info.ItemCost.Potion)
            {
                numberOfLowValueItems += c;
            }
        }

        return numberOfLowValueItems;
    }

    public static int GetCountOfItemInInventory(Item item, List<InventoryItem> inventory)
    {
        foreach (InventoryItem ii in inventory)
        {
            if (ii.item == item)
                return ii.count;
        }

        return 0;
    }
}

